import 'react-native-gesture-handler';

import React, {useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import LoginScreen from './src/screens/auth/LoginScreen';
import PlayList from './src/screens/home/Playlist';
import Tracks from './src/screens/home/Tracks';
import Details from './src/screens/home/Details';

const Stack = createStackNavigator();

const headerStyle: any = {
  headerStyle: {
    backgroundColor: '#101616',
  },
  headerTitleAlign: 'center',
  headerTitleStyle: {
    color: 'white',
  },
  headerTintColor: 'white',
};
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          options={{headerShown: false}}
          name="Login"
          component={LoginScreen}
        />
        <Stack.Screen
          options={headerStyle}
          name="PlayList"
          component={PlayList}
        />
        <Stack.Screen options={headerStyle} name="Tracks" component={Tracks} />
        <Stack.Screen
          options={headerStyle}
          name="Details"
          component={Details}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
