import React from 'react';

import LinearGradient from 'react-native-linear-gradient';

export default function Container(props: any) {
  return (
    <LinearGradient
      colors={['#053831', '#053831', '#111111']}
      style={{flex: 1}}>
      {props.children}
    </LinearGradient>
  );
}
