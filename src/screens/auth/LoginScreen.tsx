import {NavigationProp, ParamListBase, Route} from '@react-navigation/native';
import {RouteParamsI} from '@src/services/model';
import React from 'react';

import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {authorize, refresh} from 'react-native-app-auth';
import {
  AuthorizationEndpoint,
  ClientId,
  ClientSecret,
  RedirectUrl,
  TokenEndpoint,
} from '../../utils/index';
import {saveToken} from '../../utils/cache';
import {Colors} from '../../components/Colors';
import Container from '../../components/Container';
const {width, height} = Dimensions.get('window');

const spotifyAuthConfig = {
  clientId: ClientId,
  clientSecret: ClientSecret,
  redirectUrl: RedirectUrl,
  scopes: [
    'user-read-currently-playing',
    'user-read-recently-played',
    'user-read-playback-state',
    'user-top-read',
    'user-modify-playback-state',
    'streaming',
    'user-read-email',
    'user-read-private',
  ],
  serviceConfiguration: {
    authorizationEndpoint: AuthorizationEndpoint,
    tokenEndpoint: TokenEndpoint,
  },
};
interface Props {
  route: Route<string, RouteParamsI>;
  navigation: NavigationProp<ParamListBase>;
}
function LoginScreen(props: Props) {
  const handleSpotifyLogin = async () => {
    try {
      const result = await authorize(spotifyAuthConfig);
      console.log(result);
      saveToken({type: 'token', token: result.accessToken});
      props.navigation.navigate('PlayList');
    } catch (error) {
      console.log(JSON.stringify(error));
    }
  };
  return (
    <Container>
      <View style={styles.container}>
        <Text style={styles.headerText}>Spotify Login</Text>
        <TouchableOpacity
          style={styles.btnContainer}
          onPress={handleSpotifyLogin}>
          <Text style={styles.loginText}>Login</Text>
        </TouchableOpacity>
      </View>
    </Container>
  );
}
export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    position: 'absolute',
    top: 100,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'green',
    fontSize: 30,
  },
  loginText: {
    fontSize: 20,
    fontWeight: '600',
    letterSpacing: 1,
  },
  btnContainer: {
    backgroundColor: '#1FDF64',
    width: width * 0.7,
    height: 50,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
