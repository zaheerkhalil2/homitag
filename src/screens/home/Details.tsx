import {
  Dimensions,
  ImageBackground,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React from 'react';
const {width, height} = Dimensions.get('window');
import {Colors} from '../../components/Colors';
import {
  NavigationProp,
  ParamListBase,
  RouteProp,
} from '@react-navigation/native';
import Container from '../../components/Container';
interface Props {
  route: RouteProp<{params: {}}>;
  navigation: NavigationProp<ParamListBase>;
}
const Detail = (props: Props) => {
  const {img, name, duration_ms} = props.route.params as any;
  const duration = (duration_ms / 60000).toFixed(2).split('.');

  return (
    <Container>
      <ImageBackground source={{uri: img}} style={styles.img}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{name}</Text>
          <Text style={styles.title}>{duration[0] + ':' + duration[1]}</Text>
        </View>
      </ImageBackground>
    </Container>
  );
};

export default Detail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.Primary,
  },
  img: {
    width: width,
    height: height / 2,
    justifyContent: 'flex-end',
    paddingLeft: 20,
    paddingBottom: 20,
    marginVertical: 100,
  },
  titleContainer: {
    backgroundColor: 'black',
    width: width / 2,
    paddingLeft: 10,
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
});
