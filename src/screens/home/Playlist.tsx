import {
  Image,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  NavigationProp,
  ParamListBase,
  RouteProp,
} from '@react-navigation/native';

import {getPlayList} from '../../services/restApis';
import {PlayListResponse} from '../../services/model';
import Container from '../../components/Container';

interface Props {
  route: RouteProp<{params: {}}>;
  navigation: NavigationProp<ParamListBase>;
}

const Playlist = (props: Props) => {
  const [playList, setPlayList] = useState<PlayListResponse>(
    {} as PlayListResponse,
  );

  useEffect(() => {
    getUserPlayList();
  }, []);

  const getUserPlayList = async () => {
    var response = {};
    response = await getPlayList();
    setPlayList(response as PlayListResponse);
  };
  const renderPlayList = (item: any, index: number) => {
    return (
      <TouchableOpacity
        onPress={() =>
          props.navigation.navigate('Tracks', {
            ...item,
          })
        }
        key={index}
        style={styles.listContainer}>
        <Image source={{uri: item.images[0]?.url}} style={styles.imgStyle} />
        <View style={{marginHorizontal: 20}}>
          <Text style={styles.title}>{item?.artists[0]?.name}</Text>
          <Text style={styles.name}>{item?.name}</Text>
          <Text style={styles.name}>{item?.total_tracks}</Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <Container>
      <ScrollView scrollEnabled={true}>
        {playList?.albums?.items?.map((item, index) => {
          return renderPlayList(item, index);
        })}
      </ScrollView>
    </Container>
  );
};

export default Playlist;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  name: {
    color: 'white',
    fontSize: 16,
    marginTop: 5,
  },

  imgStyle: {
    width: 60,
    height: 60,
  },
  listContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginTop: 20,
  },
});
