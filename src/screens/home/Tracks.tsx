import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  NavigationProp,
  ParamListBase,
  RouteProp,
} from '@react-navigation/native';

import {getTrackList} from '../../services/restApis';
import {Colors} from '../../components/Colors';
import Container from '../../components/Container';
interface Props {
  route: RouteProp<{params: {}}>;
  navigation: NavigationProp<ParamListBase>;
}
const Tracks = (props: Props) => {
  const {id} = props.route.params as any;
  const [tracks, setTracks] = useState({} as any);
  useEffect(() => {
    getTracks();
  }, []);
  const getTracks = async () => {
    const response = await getTrackList(id);
    setTracks(response);
  };
  const renderTrackList = (item: any, index: number) => {
    const duration = (item?.duration_ms / 60000).toFixed(2).split('.');
    return (
      <TouchableOpacity
        onPress={() =>
          props.navigation.navigate('Details', {
            img: tracks?.images[0]?.url,
            ...item,
          })
        }
        key={index}
        style={styles.listContainer}>
        <Image source={{uri: tracks?.images[0]?.url}} style={styles.imgStyle} />
        <View style={{marginHorizontal: 20}}>
          <Text style={styles.title}>{item?.name}</Text>
          <Text style={styles.title}>{duration[0] + ':' + duration[1]}</Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <Container>
      <ScrollView scrollEnabled={true}>
        {tracks?.tracks?.items.map((item: any, index: number) => {
          return renderTrackList(item, index);
        })}
      </ScrollView>
    </Container>
  );
};

export default Tracks;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Primary,
  },
  title: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  name: {
    color: 'white',
    fontSize: 16,
    marginTop: 5,
  },

  imgStyle: {
    width: 60,
    height: 60,
  },
  listContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginTop: 20,
  },
});
