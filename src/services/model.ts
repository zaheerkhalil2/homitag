export interface RouteParamsI {
  key: string;
  name: string;
  params?: any;
  path: undefined;
}

export interface PlayListResponse {
  albums: {
    href: string;
    items: {
      album_type: string;
      artists: {
        external_urls: {
          spotify: string;
        };
        href: string;
        id: string;
        name: string;
        type: string;
        uri: string;
      }[];
      available_markets: string[];
      external_urls: {
        spotify: string;
      };
      href: string;
      id: string;
      images: {
        height: number;
        url: string;
        width: number;
      }[];

      name: string;
      release_date: Date;
      release_date_precision: string;
      total_tracks: number;
      type: string;
      uri: string;
    }[];
  };
}
