import {getToken} from '../utils/cache';

const getPlayList = () => {
  return fetch('https://api.spotify.com/v1/browse/new-releases?country=PK', {
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
    method: 'GET',
  })
    .then(response => response.json())
    .then(result => result);
};

const getTrackList = (id: string) => {
  return fetch(`https://api.spotify.com/v1/albums/${id}`, {
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
    method: 'GET',
  })
    .then(response => response.json())
    .then(result => result);
};

export {getPlayList, getTrackList};
