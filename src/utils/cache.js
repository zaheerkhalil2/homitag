const cacheFunc = {};

const saveToken = obj => {
  cacheFunc[obj.type] = obj.token;
};

const getToken = () => {
  return cacheFunc['token'];
};

export {saveToken, getToken};
