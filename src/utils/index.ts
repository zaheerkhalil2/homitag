const ClientId = '8af0541677ff4e0a950fcac228659b29';
const ClientSecret = '920c5f80345844a0abe5a631b8ed5666';
const RedirectUrl = 'com.homitag://oauthredirect';
const AuthorizationEndpoint = 'https://accounts.spotify.com/authorize';
const TokenEndpoint = 'https://accounts.spotify.com/api/token';

export {
  ClientId,
  ClientSecret,
  RedirectUrl,
  AuthorizationEndpoint,
  TokenEndpoint,
};
